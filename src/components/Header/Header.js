import React from 'react';
import { NavLink } from 'react-router-dom';

import './Header.css';

const Header = () => {
  return (
    <div className="App">
      <header className="header">
        <div className="container clearfix">
          <NavLink to="/" className="logo"><b className="logo-bold">Alexandr</b>Star</NavLink>
          <nav className="main-nav">
            <ul>
              <li><NavLink to="/">Home</NavLink></li>
              <li><NavLink to="/about">About us</NavLink></li>
              <li className="sub"><NavLink to="/works">Works</NavLink>
                <ul className="sub-nav">
                  <li><NavLink to="/works/game">Game</NavLink></li>
                  <li><NavLink to="/works/page-proofs">Page-proofs</NavLink></li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    </div>
  );
};

export default Header;