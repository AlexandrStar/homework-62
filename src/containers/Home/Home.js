import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import Header from "../../components/Header/Header";

import './Home.css';

class Home extends Component {


  render() {
    return (
      <div className="home">
        <Header />
        <h3>Home</h3>
        <div className="container">
          <div className="fon">
            <div className="form-block-1">
              <form>
                <h4>SIGN IN TO YOUR ACCOUNT</h4>
                <div className="form-row">
            <span className="inp-field user">
              <input type="text" id="username" placeholder="Username"/>
              <label htmlFor="username"></label>
            </span>
                  <span className="inp-field lock">
              <input type="password" id="password" placeholder="password"/>
              <label htmlFor="password"></label>
            </span>
                  <button>SIGN IN</button>
                  <p>Not a member? <Link to="/">Sign Up Now</Link></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;