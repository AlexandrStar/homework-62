import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import './Page-proofs.css';
import '../../components/PageProofs/css/style.css'
import '../../components/PageProofs/css/reset.css'
import '../../components/PageProofs/css/media.css'
import Header from "../../components/Header/Header";

import  img1 from "../../components/PageProofs/images/photo-1.jpg";
import  img2 from "../../components/PageProofs/images/photo-2.jpg";
import  img3 from "../../components/PageProofs/images/photo-3.jpg";
import  img4 from "../../components/PageProofs/images/photo-4.jpg";

class PageProofs extends Component {
  render() {
    return (
      <div className="Page-proofs">
        <Header/>
        <h3>Page-proofs</h3>
        <div className="proofs">
          <header>
            <div className="container">
              <div className="head">
                <Link to="/" >Site Name</Link>
              </div>
            </div>
          </header>
          <div className="banner">
            <div className="container">
              <div className="txt">
                <h1><Link to="/">Lorem ipsum laro
                  dolore sit amet vivamus</Link></h1>
                <p>Lorem ipsum laro sit amet
                  dolore sit ame</p>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="main-title">
              <h2>Lorem ipsum dolor sit amet</h2>
              <p>Donec odio uisque volutpat mattis eros ullam malesuada erat ut turpis.</p>
            </div>
          </div>
          <div className="content">
            <div className="container">
              <div className="items">
                <div className="item item-1">
                  <div className="inner">
                    <i className="icon desktop"></i>
                    <h3>Nunc dignissim</h3>
                    <p>Praesent dapibus, neque id curs
                      us faucibus, tortor neque egestas augue, eu vulputate magn</p>
                    <Link to="/">More</Link>
                  </div>
                </div>
                <div className="item item-2">
                  <div className="inner">
                    <i className="icon flask"></i>
                    <h3>Aliquam tinci</h3>
                    <p>Donec consectetuer ligula v
                      tate sem tristique cursus. Nam nulla quam, gravida info</p>
                    <Link to="/">More</Link>
                  </div>
                </div>
                <div className="item item-3">
                  <div className="inner">
                    <i className="icon cogs"></i>
                    <h3>Vestibu auctor</h3>
                    <p>Jorbi rbi est est, blandit sit amet,
                      sagittis vel, euismod vel, velit. Pellentesque egestas sem.</p>
                    <Link to="/">More</Link>
                  </div>
                </div>
                <div className="item item-4">
                  <div className="inner">
                    <i className="icon bullhorn"></i>
                    <h3>Civamus vesti</h3>
                    <p>Morbi est est, blandit sit amet,
                      sagittis vel, euismod vel, velit. Pellentesque egestas sem.</p>
                    <Link to="/">More</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="portfolio">
              <div className="text">
                <h4>Cras ornare tristique elit</h4>
                <p className="discription">Donec odio uisque volutpat mattis eros ullam malesuada erat ut turpis.</p>
              </div>
              <div className="folios">
                <div className="box">
                  <img src={img1} alt="apelsin"/>
                </div>
                <div className="box">
                  <img src={img2} alt="apelsin"/>
                </div>
                <div className="box">
                  <img src={img3} alt="apelsin"/>
                </div>
                <div className="box">
                  <img src={img4} alt="apelsin"/>
                </div>
              </div>
              <div className="link">
                <Link to="/">View Portfolio</Link>
              </div>
            </div>
          </div>
          <footer>
            <div className="container">
              <p>Designed by: <Link to="/">www.alltemplateneeds.com</Link> / Images from: <Link
                to="/">www.wallpaperswide.com</Link> / <Link to="/">www.photorack.net</Link> Copyright(c) website name. </p>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default PageProofs;