import React, {Component} from 'react';
import Header from "../../components/Header/Header";

import './About.css';

class About extends Component {
  render() {
    return (
      <div className="about">
        <Header/>
        <h3>About us</h3>
        <div className="container">
          <div className="list-1">
            <h1>Excellent for Mobile Devices.</h1>
            <p>Qisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea
              commodo consequat. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius claritas.
              Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
            <ul className="arrow">
              <li>Nam liber tempor cum soluta nobis eleifend option;</li>
              <li>Option congue nihil imperdiet doming id quod mazim placerat facer;</li>
              <li>Eodem modo typi, qui nunc nobis videntur parum futurum;</li>
              <li>Investigationes demonstraverunt lectores</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default About;