import React, {Component} from 'react';
import Header from "../../components/Header/Header";
import {NavLink} from "react-router-dom";

import './Works.css';

class Works extends Component {
  render() {
    return (
      <div className="works">
        <Header/>
        <h3>Works</h3>
        <div className="work-item">
        <NavLink className="link-work games" to="/works/game">Game</NavLink>
        <NavLink className="link-work pages" to="/works/page-proofs">Page-proofs</NavLink>
        </div>
      </div>
    );
  }
}

export default Works;