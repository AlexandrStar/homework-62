import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import './App.css';

import Home from "./containers/Home/Home";
import About from "./containers/About/About";
import Works from "./containers/Works/Works";
import Game from "./containers/Game/Game";
import PageProofs from "./containers/Page-proofs/Page-proofs";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/works" exact component={Works} />
          <Route path="/works/game" exact component={Game} />
          <Route path="/works/page-proofs" exact component={PageProofs} />
          <Route render={() => <h1>No found!</h1>} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
